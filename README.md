# Custom PHP DateTime types library

This library provides custom data types for date and datetime.

## Installation

via composer: 
```sh
composer require mtichy/datetime
```
or you can just download project source files directly

## Usage

with composer: 
```php
<?php
require __DIR__.'/vendor/autoload.php';
```

without composer 
```php
<?php
require 'path-to-datetime-dir/autoload.php';
```
