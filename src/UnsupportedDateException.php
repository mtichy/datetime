<?php

namespace MTi;


class UnsupportedDateException
    extends \RuntimeException
{
    public function __construct($value)
    {
        parent::__construct(sprintf(
            "Value '%s' cannot be recognized as date."
          , is_object($value)
                ? 'instance of ' . get_class($value)
                : $value
        ));
    }
}
