<?php

namespace MTi\DateTime;

use MTi\IDate;
use MTi\InvalidDateException;
use MTi\UnsupportedDateException;


abstract class DateBase
    implements IDate, \JsonSerializable
{
    /**
     * @param mixed $global Day number | Unix timestamp | Date string in acceptable format
     * @param int|null $month Month number
     * @param int|null $year Year
     * @throws InvalidDateException Invalid combination of day, month and year.
     * @throws \InvalidArgumentException Empty first argument.
     * @throws UnsupportedDateException Invalid date string format.
     */
    public function __construct($global, int $month = null, int $year = null)
    {
        if (!$global) {
            throw new \InvalidArgumentException(
                'No data to create a date from.'
            );
        }
        if ($global instanceof DateBase) {
            $year = $global->year();
            $month = $global->month();
            $global = $global->day();
        }
        if ($month === null || $year === null) {
            if (strpos($global, '-')) {
                list($year, $month, $day) = explode('-', $global);
                if (strlen($day) == 11 && substr($day, -9) == ' 00:00:00') {
                    $day = substr($day, 0, 2);
                }
            }
            elseif (strpos($global, '.')) {
                list($day, $month, $year) = explode('.', $global);
            }
            elseif (strpos($global, '/')) {
                list($month, $day, $year) = explode('/', $global);
            }
            elseif (!preg_match('~[./-]~', $global)) {
                if (is_numeric($global)) {
                    $global = intval($global);
                    $year = date('Y', $global);
                    $month = date('n', $global);
                    $day = date('j', $global);
                } else {
                    throw new UnsupportedDateException($global);
                }
            }
            else {
                throw new UnsupportedDateException($global);
            }
            if (!ctype_digit($year) || !ctype_digit($month) || !ctype_digit($day)) {
                throw new UnsupportedDateException($global);
            }
            $year = intval($year);
            $month = intval($month);
            $day = intval($day);
        }
        elseif ($month != null && $year != null) {
            $day = intval($global);
        }
        else {
            $day = 1;
        }
        self::setYear($year);
        self::setMonth($month);
        $this->setDay($day);
    }
    protected $day;
    protected $month;
    protected $year;

    /**
     * @return string
     * @throws InvalidDateException
     */
    protected function unixTimestamp(): string
    {
        $rv = strtotime(sprintf(
            '%d-%d-%d'
          , $this->year
          , $this->month
          , $this->day
        ));
        if ($rv) {
            return $rv;
        }
        throw new InvalidDateException("Date too high for timestamp!");
    }

    /**
     * {@inheritdoc}
     */
    public function day(): int
    {
        return $this->day;
    }

    /**
     * {@inheritdoc}
     */
    public function month(): int
    {
        return $this->month;
    }

    /**
     * {@inheritdoc}
     */
    public function year(): int
    {
        return $this->year;
    }

    /**
     * {@inheritdoc}
     */
    public function quarter(): int
    {
        return ceil($this->month() / 3);
    }

    /**
     * {@inheritdoc}
     */
    public function quarterRoman(): string
    {
        return [
            1 => 'I',
            2 => 'II',
            3 => 'III',
            4 => 'IV',
        ][$this->quarter()];
    }

    /**
     * {@inheritdoc}
     */
    public function setMonth(int $month): IDate
    {
        if ($month > 12) {
            throw new InvalidDateException('Invalid month in Date (More than 12?).');
        }
        if ($month < 1) {
            throw new InvalidDateException('Invalid month in Date (Less than 1?).');
        }
        $this->month = $month;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function setYear(int $year): IDate
    {
        $year = intval($year);
        if ($year < 1) {
            throw new InvalidDateException('Invalid year in Date (Less than 1?).');
        }
        $this->year = $year;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function equals(IDate $date): bool
    {
        return $this->day == $date->day()
            && $this->month == $date->month()
            && $this->year == $date->year()
        ;
    }

    protected function toDatestamp(IDate $d): int
    {
        return (int) sprintf(
            '%d%02d%02d'
          , $d->year()
          , $d->month()
          , $d->day()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function isEarlierThan(IDate $date): bool
    {
        return $this->toDatestamp($this) < $this->toDatestamp($date);
    }

    /**
     * {@inheritdoc}
     */
    public function isLaterThan(IDate $date): bool
    {
        return $this->toDatestamp($this) > $this->toDatestamp($date);
    }

    /**
     * {@inheritdoc}
     */
    public function isEarlierThanOrEquals(IDate $date): bool
    {
        return ($this->isEarlierThan($date) || $this->equals($date));
    }

    /**
     * {@inheritdoc}
     */
    public function isLaterThanOrEquals(IDate $date): bool
    {
        return ($this->isLaterThan($date) || $this->equals($date));
    }

    /**
     * {@inheritdoc}
     */
    public function isBetween(IDate $dateFrom, IDate $dateTo): bool
    {
        return $this->isLaterThanOrEquals($dateFrom)
            && $this->isEarlierThanOrEquals($dateTo)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function isBetweenExceptEquals(IDate $since, IDate $till): bool
    {
        return $this->isLaterThan($since) && $this->isEarlierThan($till);
    }

    /**
     * {@inheritdoc}
     */
    public function addMonth(): IDate
    {
        return $this->addMonths(1);
    }

    /**
     * {@inheritdoc}
     */
    public function subMonth(): IDate
    {
        return $this->subMonths(1);
    }

    /**
     * {@inheritdoc}
     */
    public function addMonths(int $months): IDate
    {
        if (0 > $months) {
            return $this->subMonths(-$months);
        }
        $this->month += $months;
        while ($this->month > 12) {
            $this->year++;
            $this->month -= 12;
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function subMonths(int $months): IDate
    {
        if (0 > $months) {
            return $this->addMonths(-$months);
        }
        $months = intval($months);
        $this->month -= $months;
        while ($this->month < 1) {
            $this->year--;
            $this->month += 12;
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addYear(): IDate
    {
        return $this->addYears(1);
    }

    /**
     * {@inheritdoc}
     */
    public function subYear(): IDate
    {
        return $this->subYears(1);
    }

    /**
     * {@inheritdoc}
     */
    public function addYears(int $years): IDate
    {
        $this->year += $years;
        return $this;
    }

    /**
     * @param int $years
     * @return IDate Self
     */
    public function subYears(int $years): IDate
    {
        $this->year -= $years;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function fitToSpecificDay(int $specificDay): IDate
    {
        if ($this->day > $specificDay) {
            $this->addMonth();
        }
        return $this->setDay($specificDay);
    }

    /**
     * {@inheritdoc}
     */
    public function fitToSpecificDayNextMonth(int $specificDay): IDate
    {
        return $this->setDay($specificDay)->addMonth();
    }

    /**
     * {@inheritdoc}
     */
    public function fitToNextSpecificDay(int $specificDay): IDate
    {
        return $this->addDay()->fitToSpecificDay($specificDay);
    }

    /**
     * {@inheritdoc}
     */
    public function fitToLastSpecificDay(int $specificDay): IDate
    {
        return $this->subMonth()->fitToSpecificDay($specificDay);
    }

    /**
     * {@inheritdoc}
     */
    public function fitToLastValidDay(): IDate
    {
        return $this->subMonth()->fitToSpecificDay($this->lastValidDay());
    }

    /**
     * {@inheritdoc}
     */
    public function isoDate(): string
    {
        return sprintf('%04d-%02d-%02d', $this->year, $this->month, $this->day);
    }

    /**
     * {@inheritdoc}
     */
    public function isoDateWithTime(): string
    {
        return sprintf('%s 00:00:00', $this->isoDate());
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        if ($this->isInfinite() || $this->isZero()) {
            return '';
        }
        return $this->isoDate();
    }

    /**
     * {@inheritdoc}
     */
    public function asPhpDateTime(): DateTime
    {
        try {
            return new DateTime($this->isoDate());
        }
        catch (\Exception $e) {
            throw new \LogicException();
        }
    }

    /**
     * @return IDate
     */
    public function cloneMe(): IDate
    {
        return clone $this;
    }

    public function __toString()
    {
        return $this->isoDate();
    }
}
