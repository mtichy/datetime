<?php

namespace MTi\DateTime;


class DateTime
    extends \DateTime
{
    public static function fromDateTime(\DateTime $dateTime)
    {
        try {
            return new static($dateTime->format('c'));
        }
        catch (\Exception $e) {
            throw new \LogicException();
        }
    }

    public function __toString()
    {
        return $this->format('c');
    }
}
