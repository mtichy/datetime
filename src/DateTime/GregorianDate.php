<?php

namespace MTi\DateTime;


class GregorianDate
    extends JulianDate
{
    /**
     * @param int $year
     * @return bool
     */
    public static function isLeapYear(int $year): bool
    {
        if (!($year % 100)) {
            return $year <= 1600 || !($year % 400);
        }
        return !($year % 4);
    }

    protected function monthDays(): int
    {
        return cal_days_in_month(CAL_GREGORIAN, $this->month(), $this->year());
    }
}
