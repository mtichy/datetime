<?php

namespace MTi\DateTime;

use MTi\IDate;
use MTi\InvalidDateException;
use MTi\UnsupportedDateException;


class JulianDate
    extends DateBase
{
    public const DATE_INFINITY = 'infinity';
    public const DATE_ZERO = 'zero';


    /**
     * @return JulianDate
     */
    public static function getZeroDate(): JulianDate
    {
        try {
            return new self(self::DATE_ZERO);
        }
        catch (InvalidDateException|UnsupportedDateException $e) {
            throw new \LogicException();
        }
    }

    /**
     * @return JulianDate
     */
    public static function getInfinityDate(): JulianDate
    {
        try {
            return new self(self::DATE_INFINITY);
        }
        catch (InvalidDateException $e) {
            throw new \LogicException();
        }
        catch (UnsupportedDateException $e) {
            throw new \LogicException();
        }
    }

    /**
     * @param \DateTime $dt|null
     * @return JulianDate|null
     */
    public static function fromDateTime(\DateTime $dt = NULL): ?JulianDate
    {
        if (is_null($dt)) {
            return NULL;
        }
        try {
            return new JulianDate($dt->format('Y-n-j'));
        }
        catch (InvalidDateException|UnsupportedDateException $e) {
            throw new \LogicException();
        }
    }

    /**
     * @param IDate[] $dates
     * @return self
     */
    public static function max(IDate ...$dates): IDate
    {
        /** @var IDate $max */
        $max = self::fromDateTime(max(
            array_map(
                function (IDate $v)
                {
                    return $v->asPhpDateTime();
                }
              , $dates
            )
        ));
        return $max;
    }

    /**
     * @param IDate[] $dates
     * @return self
     */
    public static function min(IDate ...$dates): IDate
    {
        /** @var IDate $min */
        $min = JulianDate::fromDateTime(min(
            array_map(
                function (IDate $v)
                {
                    return $v->asPhpDateTime();
                }
              , $dates
            )
        ));
        return $min;
    }


    /**
     * @param mixed $global Day number | Unix timestamp | "zero" | "infinity" | Date string in acceptable format
     * @param int|null $month Month number
     * @param int|null $year Year
     * @throws InvalidDateException Invalid combination of day, month and year.
     * @throws \InvalidArgumentException Empty first argument.
     * @throws UnsupportedDateException Invalid date string format.
     */
    public function __construct($global, int $month = null, int $year = null)
    {
        if ($global == self::DATE_INFINITY) {
            parent::__construct(31, 12, 9999);
        }
        elseif ($global == self::DATE_ZERO) {
            parent::__construct(1, 1, 1900);
        }
        else {
            parent::__construct($global, $month, $year);
        }
    }

    protected function monthDays(): int
    {
        return cal_days_in_month(CAL_JULIAN, $this->month(), $this->year());
    }

    /**
     * {@inheritdoc}
     */
    public function setDay(int $day): IDate
    {
        $maxDay = $this->monthDays();
        if ($day > $maxDay) {
            throw new InvalidDateException(sprintf(
                'Invalid day in date (%d > %d, month %d, year %d).'
              , $day
              , $maxDay
              , $this->month()
              , $this->year()
            ));
        }
        if ($day < 1) {
            throw new InvalidDateException(sprintf(
                'Invalid day in date (Day %d).'
              , $day
            ));
        }
        $this->day = $day;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addDay(): IDate
    {
        if ($this->day == $this->monthDays()) {
            $this->addMonth();
            $this->day = 1;
        } else {
            $this->day++;
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function subDay(): IDate
    {
        if ($this->day == 1) {
            $this->subMonth();
            $this->day = $this->monthDays();
        } else {
            $this->day--;
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addDays(int $days): IDate
    {
        if (0 > $days) {
            return $this->subDays(-$days);
        }
        try {
            if ($this->monthDays() - $this->day() >= $days) {
                $this->setDay($this->day() + $days);
            }
            else {
                $days -= $this->monthDays() - $this->day() + 1;
                $this->setDay(1);
                $this->addMonth();
                $this->addDays($days);
            }
        }
        catch (InvalidDateException $d) {
            throw new \LogicException();
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function subDays(int $days): IDate
    {
        if (0 > $days) {
            return $this->addDays(-$days);
        }
        try {
            if ($this->day() > $days) {
                $this->setDay($this->day() - $days);
            }
            else {
                $days -= $this->day();
                $this->subMonth();
                $this->setDay($this->monthDays());
                $this->subDays($days);
            }
        }
        catch (InvalidDateException $e) {
            throw new \LogicException();
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addMonths(int $months): IDate
    {
        parent::addMonths($months);
        return $this->fitLastDay();
    }

    /**
     * {@inheritdoc}
     */
    public function subMonths(int $months): IDate
    {
        parent::subMonths($months);
        return $this->fitLastDay();
    }

    /**
     * {@inheritdoc}
     */
    public function addYears(int $years): IDate
    {
        parent::addYears($years);
        return $this->fitLastDay();
    }

    /**
     * {@inheritdoc}
     */
    public function subYears(int $years): IDate
    {
        parent::subYears($years);
        return $this->fitLastDay();
    }

    /**
     * @return IDate
     */
    private function fitLastDay(): IDate
    {
        $lastValidDay = $this->lastValidDay();
        if ($this->day() > $lastValidDay) {
            try {
                $this->setDay($lastValidDay);
            }
            catch (InvalidDateException $e) {
                throw new \LogicException();
            }
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function lastValidDay(): int
    {
        return $this->monthDays();
    }

    /**
     * {@inheritdoc}
     */
    public function setMonth(int $val): IDate
    {
        parent::setMonth($val);
        return $this->fitLastDay();
    }

    /**
     * {@inheritdoc}
     */
    public function setYear(int $val): IDate
    {
        parent::setYear($val);
        return $this->fitLastDay();
    }

    /**
     * {@inheritdoc}
     */
    public function weekDay(): int
    {
        $gd = getdate($this->unixTimestamp());
        return $gd['wday'] == 0 ? 7 : $gd['wday'];
    }

    /**
     * {@inheritdoc}
     */
    public function setZero(): IDate
    {
        try {
            $this
                ->setYear(1900)
                ->setMonth(1)
                ->setDay(1)
            ;
        }
        catch (InvalidDateException $e) {
            throw new \LogicException();
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isZero(): bool
    {
        return 1900 == $this->year()
            && 1 == $this->month()
            && 1 == $this->day()
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function setInfinite(): IDate
    {
        try {
            $this
                ->setYear(9999)
                ->setMonth(12)
                ->setDay(31)
            ;
        }
        catch (InvalidDateException $e) {
            throw new \LogicException();
        }
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function isInfinite(): bool
    {
        return 9999 == $this->year()
            && 12 == $this->month()
            && 31 == $this->day()
        ;
    }
}
