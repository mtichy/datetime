<?php

namespace MTi\DateTime;

use DateTimeZone;
use MTi\InvalidDateException;
use MTi\UnsupportedDateException;
use MTi\UnsupportedMonthException;


class GregorianMonth
    implements \JsonSerializable
{
    public const MONTH_INFINITY = 'infinity';
    public const MONTH_ZERO = 'zero';


    public static function getZeroMonth(): GregorianMonth
    {
        try {
            return new self(self::MONTH_ZERO);
        }
        catch (UnsupportedMonthException $e) {
            throw new \LogicException();
        }
    }

    public static function getInfinityMonth(): GregorianMonth
    {
        try {
            return new self(self::MONTH_INFINITY);
        }
        catch (UnsupportedMonthException $e) {
            throw new \LogicException();
        }
    }

    /**
     * @param \DateTime $dt |null
     * @return GregorianMonth|null
     */
    public static function fromDateTime(\DateTime $dt = NULL): ?GregorianMonth
    {
        if (is_null($dt)) {
            return NULL;
        }
        try {
            return new GregorianMonth($dt->format('n/y'));
        }
        catch (UnsupportedMonthException $e) {
            throw new \LogicException();
        }
    }

    /**
     * @param mixed $global Month nr. | Month identifier | "zero" | "infinity"
     * @param int|null $year Year
     * @throws \InvalidArgumentException If first argument empty
     * @throws UnsupportedMonthException If wrong format
     */
    public function __construct($global, int $year = NULL)
    {
        if (is_null($global)) {
            throw new \InvalidArgumentException('No data to create a month from.');
        }
        if ($global == self::MONTH_INFINITY) {
            $this->setMonth(12);
            $this->setYear(9999);
        }
        elseif ($global == self::MONTH_ZERO) {
            $this->setMonth(1);
            $this->setYear(2000);
        }
        else {
            if ($year) {
                if (!is_int($global)) {
                    throw new UnsupportedMonthException($global);
                }
                $month = $global;
            }
            else {
                if (!preg_match('/^(0?[1-9]|1[012])\/\d{2}$/', $global)) {
                    throw new UnsupportedMonthException($global);
                }
                $parts = explode('/', $global);
                $month = intval($parts[0]);
                $year = 2000 + intval($parts[1]);

            }
            $this->setMonth($month);
            $this->setYear($year);
        }
    }
    protected $month;
    protected $year;

    public function month(): int
    {
        return $this->month;
    }

    public function year(): int
    {
        return $this->year;
    }

    /**
     * @param int $month
     * @return GregorianMonth
     * @throws UnsupportedMonthException
     */
    public function setMonth(int $month): GregorianMonth
    {
        if ($month > 12) {
            throw new UnsupportedMonthException(
                'Invalid month in Date (More than 12?).'
            );
        }
        if ($month < 1) {
            throw new UnsupportedMonthException(
                'Invalid month in Date (Less than 1?).'
            );
        }
        $this->month = $month;
        return $this;
    }


    /**
     * @param int $year
     * @return GregorianMonth
     * @throws UnsupportedMonthException
     */
    public function setYear(int $year): GregorianMonth
    {
        $year = intval($year);
        if ($year < 1) {
            throw new UnsupportedMonthException(
                'Invalid year in Date (Less than 1?).'
            );
        }
        $this->year = $year;
        return $this;
    }

    public function equals(GregorianMonth $date): bool
    {
        return $this->month == $date->month()
            && $this->year == $date->year()
        ;
    }

    protected function toMonthstamp(GregorianMonth $d): int
    {
        return (int)sprintf(
            '%d%02d'
          , $d->year()
          , $d->month()
        );
    }

    public function isEarlierThan(GregorianMonth $date): bool
    {
        return $this->toMonthstamp($this) < $this->toMonthstamp($date);
    }

    public function isLaterThan(GregorianMonth $date): bool
    {
        return $this->toMonthstamp($this) > $this->toMonthstamp($date);
    }

    public function isEarlierThanOrEquals(GregorianMonth $date): bool
    {
        return ($this->isEarlierThan($date) || $this->equals($date));
    }

    public function isLaterThanOrEquals(GregorianMonth $date): bool
    {
        return ($this->isLaterThan($date) || $this->equals($date));
    }

    public function isBetween(GregorianMonth $dateFrom, GregorianMonth $dateTo): bool
    {
        return $this->isLaterThanOrEquals($dateFrom)
            && $this->isEarlierThanOrEquals($dateTo)
        ;
    }

    public function isBetweenExceptEquals(GregorianMonth $since, GregorianMonth $till): bool
    {
        return $this->isLaterThan($since) && $this->isEarlierThan($till);
    }

    public function addMonth(): GregorianMonth
    {
        return $this->addMonths(1);
    }

    public function subMonth(): GregorianMonth
    {
        return $this->subMonths(1);
    }

    public function addMonths(int $months): GregorianMonth
    {
        if (0 > $months) {
            return $this->subMonths(-$months);
        }
        $this->month += $months;
        while ($this->month > 12) {
            $this->year++;
            $this->month -= 12;
        }
        return $this;
    }

    public function subMonths(int $months): GregorianMonth
    {
        if (0 > $months) {
            return $this->addMonths(-$months);
        }
        $months = intval($months);
        $this->month -= $months;
        while ($this->month < 1) {
            $this->year--;
            $this->month += 12;
        }
        return $this;
    }

    public function addYear(): GregorianMonth
    {
        return $this->addYears(1);
    }

    public function subYear(): GregorianMonth
    {
        return $this->subYears(1);
    }

    public function addYears(int $years): GregorianMonth
    {
        $this->year += $years;
        return $this;
    }

    /**
     * @param int $years
     * @return GregorianMonth Self
     */
    public function subYears(int $years): GregorianMonth
    {
        $this->year -= $years;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        if ($this->isInfinite()) {
            return '';
        }
        return sprintf('%02d/%d', $this->month(), $this->year() - 2000);
    }

    /**
     * @return GregorianMonth
     */
    public function cloneMe(): GregorianMonth
    {
        return clone $this;
    }

    public function __toString()
    {
        return sprintf('%02d/%d', $this->month(), $this->year());
    }

    public function setInfinite(): GregorianMonth
    {
        try {
            $this->setYear(9999)
                ->setMonth(12)
            ;
        }
        catch (UnsupportedMonthException $e) {
            throw new \LogicException();
        }
        return $this;
    }

    public function isInfinite(): bool
    {
        return 9999 == $this->year()
            && 12 == $this->month();
    }

    public function getFirstDay(): GregorianDate
    {
        try {
            return new GregorianDate(1, $this->month(), $this->year());
        }
        catch (InvalidDateException|UnsupportedDateException $e) {
            throw new \LogicException();
        }
    }

    public function getLastDay(): GregorianDate
    {
        try {
            return new GregorianDate($this->getFirstDay()->lastValidDay(), $this->month(), $this->year());
        }
        catch (InvalidDateException|UnsupportedDateException $e) {
            throw new \LogicException();
        }
    }

    /**
     * @param DateTimeZone $timezone
     * @return ExtendedDatetime
     * @throws \MTi\UnsupportedDateTimeException
     */
    public function getFirstSecond(DateTimeZone $timezone): ExtendedDatetime
    {
        return new ExtendedDatetime(
            sprintf('%d-%02d-01 00:00:00', $this->year(), $this->month())
          , $timezone
        );
    }

    /**
     * @param DateTimeZone $timezone
     * @return ExtendedDatetime
     * @throws \MTi\UnsupportedDateTimeException
     */
    public function getLastSecond(DateTimeZone $timezone): ExtendedDatetime
    {
        return new ExtendedDatetime($this->getLastDay()->asPhpDateTime()->format('Y-n-j ') . '23:59:59', $timezone);
    }
}
