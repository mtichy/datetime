<?php

namespace MTi\DateTime;

use DateTimeZone;
use MTi\IDate;
use MTi\IDatetime;
use MTi\InvalidDateException;
use MTi\UnsupportedDateException;
use MTi\UnsupportedDateTimeException;


class ExtendedDatetime
    extends \DateTime
    implements IDatetime
{
    public const DATETIME_INFINITY = 'infinity';


    /**
     * @param mixed $time
     * @param DateTimeZone $timezone
     * @throws \InvalidArgumentException
     * @throws UnsupportedDateTimeException
     */
    public function __construct($time, DateTimeZone $timezone)
    {
        if (is_null($time)) {
            throw new \InvalidArgumentException('No data to create a datetime from.');
        }
        if ($time instanceof IDatetime) {
            try {
                parent::__construct($time->isoDatetime(), $timezone);
            }
            catch (\Exception $e) {
                throw new \LogicException();
            }
        }
        elseif ($time instanceof \DateTime) {
            try {
                parent::__construct($time->format('Y-n-j H:i:s'), $timezone);
            }
            catch (\Exception $e) {
                throw new \LogicException();
            }
        }
        elseif ($time == self::DATETIME_INFINITY) {
            try {
                parent::__construct(IDatetime::INFINITY_DATETIME, $timezone);
            }
            catch (\Exception $e) {
                throw new \LogicException();
            }
        }
        elseif (preg_match('/^[a-z]+$/i', $time)) {
            throw new \InvalidArgumentException("Only allowed init string is 'infinity'.");
        }
        else {
            try {
                parent::__construct($time, $timezone);
            }
            catch (\Exception $e) {
                throw new UnsupportedDateTimeException($e->getMessage());
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function date(): IDate
    {
        try {
            return new JulianDate($this->day(), $this->month(), $this->year());
        }
        catch (InvalidDateException|UnsupportedDateException $e) {
            throw new \LogicException();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setEndOfDay(): IDatetime
    {
        $this->setTime(23, 59, 59);
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function year(): int
    {
        return intval(parent::format('Y'));
    }

    /**
     * {@inheritdoc}
     */
    public function month(): int
    {
        return intval(parent::format('n'));
    }

    /**
     * {@inheritdoc}
     */
    public function day(): int
    {
        return intval(parent::format('j'));
    }

    /**
     * {@inheritdoc}
     */
    public function hour(): int
    {
        return intval(parent::format('H'));
    }

    /**
     * {@inheritdoc}
     */
    public function minute(): int
    {
        return intval(parent::format('i'));
    }

    /**
     * {@inheritdoc}
     */
    public function second(): int
    {
        return intval(parent::format('s'));
    }

    /**
     * @param string $separator
     * @return string
     */
    private function makeIsoDateTime(string $separator = ' '): string
    {
        return sprintf(
            '%s%s%02d:%02d:%02d'
          , $this->date()->isoDate()
          , $separator
          , $this->hour()
          , $this->minute()
          , $this->second()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function isoDatetime(): string
    {
        return $this->makeIsoDateTime();
    }

    /**
     * {@inheritdoc}
     */
    public function isInfinite(): bool
    {
        return $this->isoDatetime() == IDatetime::INFINITY_DATETIME;
    }

    public function __toString()
    {
        return $this->isoDatetime();
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        if ($this->isInfinite()) {
            return '';
        }
        return $this->makeIsoDateTime('T');
    }

    /**
     * {@inheritdoc}
     */
    public function asPhpDatetime(): DateTime
    {
        return DateTime::fromDateTime($this);
    }

    /**
     * {@inheritdoc}
     */
    public function cloneMe(): IDatetime
    {
        return clone $this;
    }
}
