<?php

namespace MTi;

use MTi\DateTime\DateTime;


interface IDatetime
    extends \JsonSerializable
{
    public const INFINITY_DATETIME = '9999-12-31 23:59:59';

    /**
     * @return int
     */
    public function year(): int;

    /**
     * @return int
     */
    public function month(): int;

    /**
     * @return int
     */
    public function day(): int;

    /**
     * @return int
     */
    public function hour(): int;

    /**
     * @return int
     */
    public function minute(): int;

    /**
     * @return int
     */
    public function second(): int;


    /**
     * @return IDatetime
     */
    public function setEndOfDay(): IDatetime;

    /**
     * @return string
     */
    public function isoDatetime(): string;

    /**
     * @return DateTime
     */
    public function asPhpDatetime(): DateTime;

    /**
     * @return IDate
     */
    public function date(): IDate;

    /**
     * @return int
     */
    public function getTimestamp();

    /**
     * @return \DateTimeZone
     */
    public function getTimeZone();

    /**
     * @return bool
     */
    public function isInfinite(): bool;

    /**
     * Adds an amount of days, months, years, hours, minutes and seconds to a DateTime object
     *
     * @param \DateInterval $interval
     * @return static
     */
    public function add($interval);

    /**
     * Subtracts an amount of days, months, years, hours, minutes and seconds from a DateTime object
     *
     * @param \DateInterval $interval
     * @return static
     */
    public function sub($interval);

    /**
     * Sets the current time of the DateTime object to a different time.
     *
     * @param int $hour
     * @param int $minute
     * @param int $second
     * @return static|false
     */
    public function setTime($hour, $minute, $second=0);

    /**
     * Sets the current date of the DateTime object to a different date.
     *
     * @param int $year
     * @param int $month
     * @param int $day
     * @return static
     */
    public function setDate($year, $month, $day);

    /**
     * Creates datetime clone.
     *
     * @return IDatetime
     */
    public function cloneMe(): IDatetime;
}
