<?php

namespace MTi;


class UnsupportedTimeZoneException
    extends \InvalidArgumentException
{
    public function __construct($value)
    {
        parent::__construct(sprintf(
            "Value '%s' cannot be recognized as timezone."
          , is_object($value)
                ? 'instance of ' . get_class($value)
                : $value
        ));
    }
}
