<?php

namespace MTi;

use MTi\DateTime\DateTime;


interface IDate
{
    /**
     * @return int
     */
    public function day(): int;

    /**
     * @return int
     */
    public function month(): int;

    /**
     * @return int
     */
    public function year(): int;

    /**
     * @return int
     */
    public function quarter(): int;

    /**
     * @return string
     */
    public function quarterRoman(): string;

    /**
     * @param int $day 0-(28-31)
     * @return IDate
     * @throws InvalidDateException Pokud den v měsící neexistuje.
     */
    public function setDay(int $day): IDate;

    /**
     * @param int $month 1-12
     * @return IDate
     * @throws InvalidDateException
     */
    public function setMonth(int $month): IDate;

    /**
     * @param int $year
     * @return IDate
     * @throws InvalidDateException
     */
    public function setYear(int $year): IDate;

    /**
     * @return IDate
     */
    public function addDay(): IDate;

    /**
     * @return IDate
     */
    public function subDay(): IDate;

    /**
     * @param int $days
     * @return IDate
     */
    public function addDays(int $days): IDate;

    /**
     * @param int $days
     * @return IDate
     */
    public function subDays(int $days): IDate;

    /**
     * @return IDate
     */
    public function addMonth(): IDate;

    /**
     * @return IDate
     */
    public function subMonth(): IDate;

    /**
     * @param int $months
     * @return IDate
     */
    public function addMonths(int $months): IDate;

    /**
     * @param int $months
     * @return IDate
     */
    public function subMonths(int $months): IDate;

    /**
     * @return IDate
     */
    public function addYear(): IDate;

    /**
     * @return IDate
     */
    public function subYear(): IDate;

    /**
     * @param int $years
     * @return IDate
     */
    public function addYears(int $years): IDate;

    /**
     * @param int $years
     * @return IDate
     */
    public function subYears(int $years): IDate;

    /**
     * @return IDate
     */
    public function setZero(): IDate;

    /**
     * @return IDate
     */
    public function setInfinite(): IDate;

    /**
     * Returns date int ISO format - YYYY-MM-DD.
     *
     * @return string
     */
    public function isoDate(): string;

    /**
     * Returns date in ISO format - including zero time - YYYY-MM-DD 00:00:00.
     *
     * @return string
     */
    public function isoDateWithTime(): string;

    /**
     * @return DateTime
     */
    public function asPhpDateTime(): DateTime;

    /**
     * Moves date to the closest next specific day
     *
     * @param int $specificDay Den 0-(28-31).
     * @return IDate
     * @throws InvalidDateException Given day, which does not exist in the result month.
     */
    public function fitToSpecificDay(int $specificDay): IDate;

    /**
     * Moves date to the chosen day of next month.
     *
     * @param int $specificDay Den 0-(28-31).
     * @return IDate
     * @throws InvalidDateException Given day, which does not exist in the result month.
     */
    public function fitToSpecificDayNextMonth(int $specificDay): IDate;

    /**
     * Moves date to the closest next specific day (even if the date is already on a specific day).
     *
     * @param int $specificDay Day 0-(28-31).
     * @return IDate
     * @throws InvalidDateException Given day, which does not exist in the result month.
     */
    public function fitToNextSpecificDay(int $specificDay): IDate;

    /**
     * Moves date backwards to given day.
     *
     * @param int $specificDay Day 0-(28-31).
     * @return IDate
     * @throws InvalidDateException Given day, which does not exist in the result month.
     */
    public function fitToLastSpecificDay(int $specificDay): IDate;

    /**
     * Moves date forward to last day of the current month.
     *
     * @return IDate
     * @throws InvalidDateException Given day, which does not exist in the result month.
     */
    public function fitToLastValidDay(): IDate;

    /**
     * @return int 1-7
     * @throws InvalidDateException
     */
    public function weekDay(): int;

    /**
     * @param IDate $date
     * @return bool
     */
    public function equals(IDate $date): bool;

    /**
     * @param IDate $date
     * @return bool
     */
    public function isEarlierThan(IDate $date): bool;

    /**
     * @param IDate $date
     * @return bool
     */
    public function isLaterThan(IDate $date): bool;

    /**
     * @param IDate $date
     * @return bool
     */
    public function isEarlierThanOrEquals(IDate $date): bool;

    /**
     * @param IDate $date
     * @return bool
     */
    public function isLaterThanOrEquals(IDate $date): bool;

    /**
     * @param IDate $dateFrom
     * @param IDate $dateTo
     * @return bool
     */
    public function isBetween(IDate $dateFrom, IDate $dateTo): bool;

    /**
     * @param IDate $since
     * @param IDate $till
     * @return bool
     */
    public function isBetweenExceptEquals(IDate $since, IDate $till): bool;

    /**
     * @return bool
     */
    public function isZero(): bool;

    /**
     * @return bool
     */
    public function isInfinite(): bool;

    /**
     * Returns last day of month number.
     *
     * @return int
     */
    public function lastValidDay(): int;

    /**
     * Creates date clone.
     *
     * @return IDate
     */
    public function cloneMe(): IDate;
}
