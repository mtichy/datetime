<?php

namespace MTi;


class UnsupportedMonthException
    extends \RuntimeException
{
    public function __construct($value)
    {
        parent::__construct(sprintf(
            "Value '%s' cannot be recognized as month."
          , is_object($value)
                ? 'instance of ' . get_class($value)
                : $value
        ));
    }
}
