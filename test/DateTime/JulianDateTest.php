<?php

namespace MTi\UnitTest;

use MTi\DateTime\DateTime;
use MTi\DateTime\JulianDate;
use MTi\InvalidDateException;
use MTi\UnsupportedDateException;
use PHPUnit\Framework\TestCase;


class JulianDateTest
    extends TestCase
{
    public function initializationPassProvider()
    {
        return [
            'infinity' => ['9999-12-31', JulianDate::DATE_INFINITY],
            'database format' => ['1920-01-31', '1920-01-31'],
            'leap february' => ['2016-02-29', '2016-02-29'],
            'M/D/Y (US) format' => ['1984-03-12', '03/12/1984'],
            'D.M.Y format' => ['2014-08-01', '1.8.2014'],
        ];
    }

    public function initializationFailProvider()
    {
        return [
            'not date' => ['invalid'],
            'unsupported' => ['22#05#1999'],
            'invalid' => ['2018-06-1a'],
        ];
    }

    private function cd($global, $month = NULL, $year = NULL)
    {
        try {
            return new JulianDate($global, $month, $year);
        }
        catch (InvalidDateException|UnsupportedDateException $e) {
            throw new \LogicException();
        }
    }

    public function testInitPass()
    {
        self::assertEquals(
            '2000-03-25'
          , $this->cd(25, 3, 2000)->isoDate()
        );
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testInitInvalidNull()
    {
        $this->cd(null);
    }

    /**
     * @expectedException \MTi\InvalidDateException
     *
     * @throws UnsupportedDateException
     */
    public function testInitInvalidDate()
    {
        new JulianDate(31, 4, 2000);
    }

    /**
     * @dataProvider initializationPassProvider
     *
     * @param $expected
     * @param string $datestring
     */
    public function testFormatsPass(string $expected, string $datestring)
    {
        self::assertEquals(
            $expected
          , ($this->cd($datestring))->isoDate()
        );
    }

    /**
     * @dataProvider initializationFailProvider
     * @expectedException \MTi\UnsupportedDateException
     *
     * @param $datestring
     * @throws InvalidDateException
     */
    public function testFormatsFail($datestring)
    {
        new JulianDate($datestring);
    }

    /**
     * @throws \PHPUnit\Framework\AssertionFailedError
     */
    public function testEquals()
    {
        $d1 = $this->cd(5, 12, 2000);
        $d2 = $this->cd(5, 12, 2000);
        $d3 = $this->cd(12, 5, 2000);
        self::assertTrue($d1->equals($d2));
        self::assertFalse($d1->equals($d3));
    }

    /**
     * @throws \PHPUnit\Framework\AssertionFailedError
     */
    public function testComparation()
    {
        $d1 = $this->cd(15, 8, 1992);
        $d1c = $this->cd(15, 8, 1992);
        $d2 = $this->cd(16, 8, 1992);

        self::assertTrue($d1->isEarlierThan($d2));
        self::assertFalse($d2->isEarlierThan($d1));

        self::assertTrue($d1->isEarlierThanOrEquals($d1c));
        self::assertTrue($d1c->isEarlierThanOrEquals($d1));
        self::assertTrue($d1->isEarlierThanOrEquals($d2));
        self::assertFalse($d2->isEarlierThanOrEquals($d1));

        self::assertFalse($d1->isLaterThan($d2));
        self::assertTrue($d2->isLaterThan($d1));

        self::assertTrue($d1->isLaterThanOrEquals($d1c));
        self::assertTrue($d1c->isLaterThanOrEquals($d1));
        self::assertTrue($d2->isLaterThanOrEquals($d1));
        self::assertFalse($d1->isLaterThanOrEquals($d2));

        self::assertEquals($d1->isoDate(), JulianDate::min($d1, $d2));
        self::assertEquals($d1->isoDate(), JulianDate::min($d2, $d1));
        self::assertEquals($d2->isoDate(), JulianDate::max($d1, $d2));
        self::assertEquals($d2->isoDate(), JulianDate::max($d2, $d1));
    }

    /**
     * @throws \PHPUnit\Framework\AssertionFailedError
     */
    public function testIntervals()
    {
        $intervalSince = $this->cd(1, 5, 1945);
        $intervalUntil = $this->cd(31, 5, 1945);
        $dx1 = $this->cd(1, 5, 1945);
        $dx2 = $this->cd(15, 5, 1945);
        $dx3 = $this->cd(31, 5, 1945);
        $dy1 = $this->cd(30, 4, 1945);
        $dy2 = $this->cd(1, 6, 1945);

        self::assertTrue($dx1->isBetween($intervalSince, $intervalUntil));
        self::assertTrue($dx2->isBetween($intervalSince, $intervalUntil));
        self::assertTrue($dx3->isBetween($intervalSince, $intervalUntil));

        self::assertFalse($dx1->isBetweenExceptEquals($intervalSince, $intervalUntil));
        self::assertTrue($dx2->isBetween($intervalSince, $intervalUntil));
        self::assertFalse($dx3->isBetweenExceptEquals($intervalSince, $intervalUntil));

        self::assertFalse($dy1->isBetween($intervalSince, $intervalUntil));
        self::assertFalse($dy2->isBetween($intervalSince, $intervalUntil));
    }

    public function testDayShift()
    {
        $d = $this->cd(29, 2, 2016);
        $dPlus = $d->addDay();
        self::assertEquals('2016-03-01', $dPlus->isoDate());
        $dMin = $d->subDay();
        self::assertEquals('2016-02-29', $dMin->isoDate());

        $d = $this->cd(31, 5, 1984);
        $dPlus = $d->addDay();
        self::assertEquals('1984-06-01', $dPlus->isoDate());
        $dMin = $d->subDay();
        self::assertEquals('1984-05-31', $dMin->isoDate());

        $d = $this->cd(30, 12, 1978);
        $dPlus = $d->addDays(3);
        self::assertEquals('1979-01-02', $dPlus->isoDate());
        $dMin = $d->subDays(5);
        self::assertEquals('1978-12-28', $dMin->isoDate());
    }

    public function testMonthShift()
    {
        $d = $this->cd(15, 4, 2019);
        $dPlus = $d->addMonth();
        self::assertEquals('2019-05-15', $dPlus->isoDate());
        $dMin = $d->subMonth();
        self::assertEquals('2019-04-15', $dMin->isoDate());

        $d = $this->cd(31, 3, 2000);
        $dPlus = $d->addMonth();
        self::assertEquals('2000-04-30', $dPlus->isoDate());
        $dMin = $d->subMonth();
        self::assertEquals('2000-03-30', $dMin->isoDate());

        $d = $this->cd(1, 12, 1912);
        $dPlus = $d->addMonths(3);
        self::assertEquals('1913-03-01', $dPlus->isoDate());
        $dMin = $d->subMonths(5);
        self::assertEquals('1912-10-01', $dMin->isoDate());
    }

    /**
     * @throws InvalidDateException
     */
    public function testFitToSpecificDay()
    {
        $d1 = $this->cd(15, 11, 2014);
        $d2 = $d1->cloneMe();
        $d3 = $d1->cloneMe();

        self::assertEquals('2014-11-15', $d1->fitToSpecificDay(15)->isoDate());
        self::assertEquals('2014-11-22', $d2->fitToSpecificDay(22)->isoDate());
        self::assertEquals('2014-12-05', $d3->fitToSpecificDay(5)->isoDate());
    }

    /**
     * @expectedException \MTi\InvalidDateException
     */
    public function testFitToSpecificDayFail()
    {
        $d = $this->cd(15, 11, 2011);
        $d->fitToSpecificDay(31);
    }

    /**
     * @throws InvalidDateException
     */
    public function testFitToNextSpecificDay()
    {
        $d1 = $this->cd(15, 11, 2014);
        $d2 = $d1->cloneMe();
        $d3 = $d1->cloneMe();

        self::assertEquals('2014-12-15', $d1->fitToNextSpecificDay(15)->isoDate());
        self::assertEquals('2014-11-22', $d2->fitToNextSpecificDay(22)->isoDate());
        self::assertEquals('2014-12-05', $d3->fitToNextSpecificDay(5)->isoDate());
    }

    /**
     * @throws InvalidDateException
     */
    public function testFitToSpecificDayNextMonth()
    {
        $d1 = $this->cd(15, 11, 2014);
        $d2 = $d1->cloneMe();
        $d3 = $d1->cloneMe();

        self::assertEquals('2014-12-15', $d1->fitToSpecificDayNextMonth(15)->isoDate());
        self::assertEquals('2014-12-22', $d2->fitToSpecificDayNextMonth(22)->isoDate());
        self::assertEquals('2014-12-05', $d3->fitToSpecificDayNextMonth(5)->isoDate());
    }

    /**
     * @throws InvalidDateException
     */
    public function testFitToLastSpecificDay()
    {
        $d1 = $this->cd(15, 11, 2014);
        $d2 = $d1->cloneMe();
        $d3 = $d1->cloneMe();

        self::assertEquals('2014-10-15', $d1->fitToLastSpecificDay(15)->isoDate());
        self::assertEquals('2014-10-22', $d2->fitToLastSpecificDay(22)->isoDate());
        self::assertEquals('2014-11-05', $d3->fitToLastSpecificDay(5)->isoDate());
    }

    /**
     * @throws InvalidDateException
     */
    public function testWeekday()
    {
        $d = $this->cd(11, 3, 1984);

        self::assertEquals(7, $d->weekDay());
        $d->addDay();
        self::assertEquals(1, $d->weekDay());
        $d->addDay();
        self::assertEquals(2, $d->weekDay());
        $d->addDay();
        self::assertEquals(3, $d->weekDay());
        $d->addDay();
        self::assertEquals(4, $d->weekDay());
        $d->addDay();
        self::assertEquals(5, $d->weekDay());
        $d->addDay();
        self::assertEquals(6, $d->weekDay());
    }

    /**
     * @throws \PHPUnit\Framework\Exception
     * @throws \Exception
     */
    public function testTransform()
    {
        $dateString = '2017-03-02';
        $dt = new DateTime($dateString);
        self::assertInstanceOf(JulianDate::class, JulianDate::fromDateTime($dt));
        self::assertNull(JulianDate::fromDateTime(null));
        self::assertEquals($dateString, JulianDate::fromDateTime($dt)->isoDate());
    }

    public function testQuarter()
    {
        self::assertEquals(1, ($this->cd(1, 1, 2000))->quarter());
        self::assertEquals(1, ($this->cd(1, 2, 2000))->quarter());
        self::assertEquals(1, ($this->cd(1, 3, 2000))->quarter());
        self::assertEquals(2, ($this->cd(1, 4, 2000))->quarter());
        self::assertEquals(2, ($this->cd(1, 5, 2000))->quarter());
        self::assertEquals(2, ($this->cd(1, 6, 2000))->quarter());
        self::assertEquals(3, ($this->cd(1, 7, 2000))->quarter());
        self::assertEquals(3, ($this->cd(1, 8, 2000))->quarter());
        self::assertEquals(3, ($this->cd(1, 9, 2000))->quarter());
        self::assertEquals(4, ($this->cd(1, 10, 2000))->quarter());
        self::assertEquals(4, ($this->cd(1, 11, 2000))->quarter());
        self::assertEquals(4, ($this->cd(1, 12, 2000))->quarter());
    }

    public function testQuarterRoman()
    {
        self::assertEquals('I', ($this->cd(1, 1, 2000))->quarterRoman());
        self::assertEquals('I', ($this->cd(1, 2, 2000))->quarterRoman());
        self::assertEquals('I', ($this->cd(1, 3, 2000))->quarterRoman());
        self::assertEquals('II', ($this->cd(1, 4, 2000))->quarterRoman());
        self::assertEquals('II', ($this->cd(1, 5, 2000))->quarterRoman());
        self::assertEquals('II', ($this->cd(1, 6, 2000))->quarterRoman());
        self::assertEquals('III', ($this->cd(1, 7, 2000))->quarterRoman());
        self::assertEquals('III', ($this->cd(1, 8, 2000))->quarterRoman());
        self::assertEquals('III', ($this->cd(1, 9, 2000))->quarterRoman());
        self::assertEquals('IV', ($this->cd(1, 10, 2000))->quarterRoman());
        self::assertEquals('IV', ($this->cd(1, 11, 2000))->quarterRoman());
        self::assertEquals('IV', ($this->cd(1, 12, 2000))->quarterRoman());
    }

    public function testCalendarSpecific()
    {
        self::assertEquals(
            '1900-02-29'
          , $this->cd(28, 2, 1900)->addDay()->isoDate()
        );
    }
}
