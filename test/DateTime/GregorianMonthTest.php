<?php

namespace MTi\UnitTest;

use MTi\DateTime\JulianDate;
use MTi\DateTime\ExtendedDatetime;
use MTi\DateTime\GregorianMonth;
use MTi\InvalidDateException;
use MTi\UnsupportedDateException;
use MTi\UnsupportedDateTimeException;
use MTi\UnsupportedMonthException;
use PHPUnit\Framework\TestCase;


class GregorianMonthTest
    extends TestCase
{
    private function createMonth($global, int $year = NULL)
    {
        try {
            return new GregorianMonth($global, $year);
        }
        catch (UnsupportedMonthException $e) {
            throw new \LogicException();
        }
    }

    public function initializationPassProvider()
    {
        return [
            'infinity' => ['12/9999', GregorianMonth::MONTH_INFINITY],
            'zero' => ['01/2000', GregorianMonth::MONTH_ZERO],
            'month indentification' => ['12/2099', '12/99'],
        ];
    }

    public function initializationFailProvider()
    {
        return [
            'not date' => ['invalid'],
            'unsupported' => ['22#05#1999'],
            'invalid' => ['2018-6'],
        ];
    }

    public function testInitPass()
    {
        self::assertEquals('02/2012', ($this->createMonth('02/12'))->__toString());
        self::assertEquals('02/2012', ($this->createMonth(2, 2012))->__toString());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testInitInvalidNull()
    {
        $this->createMonth(NULL);
    }

    /**
     * @expectedException \MTi\UnsupportedMonthException
     */
    public function testInitInvalidMonth()
    {
        new GregorianMonth(31, 2000);
    }

    /**
     * @dataProvider initializationPassProvider
     *
     * @param string $expected
     * @param string $datestring
     */
    public function testFormatsPass(string $expected, string $datestring)
    {
        self::assertEquals(
            $expected
          , ($this->createMonth($datestring))->__toString()
        );
    }

    /**
     * @dataProvider initializationFailProvider
     * @expectedException \MTi\UnsupportedMonthException
     *
     * @param string $datestring
     */
    public function testFormatsFail(string $datestring)
    {
        new GregorianMonth($datestring);
    }

    /**
     * @throws \PHPUnit\Framework\AssertionFailedError
     */
    public function testEquals()
    {
        $d1 = $this->createMonth(12, 2000);
        $d2 = $this->createMonth(12, 2000);
        $d3 = $this->createMonth(5, 2000);
        self::assertTrue($d1->equals($d2));
        self::assertFalse($d1->equals($d3));
    }

    /**
     * @throws \PHPUnit\Framework\AssertionFailedError
     */
    public function testComparation()
    {
        $d1 = $this->createMonth(8, 1992);
        $d1c = $this->createMonth(8, 1992);
        $d2 = $this->createMonth(9, 1992);

        self::assertTrue($d1->isEarlierThan($d2));
        self::assertFalse($d2->isEarlierThan($d1));

        self::assertTrue($d1->isEarlierThanOrEquals($d1c));
        self::assertTrue($d1c->isEarlierThanOrEquals($d1));
        self::assertTrue($d1->isEarlierThanOrEquals($d2));
        self::assertFalse($d2->isEarlierThanOrEquals($d1));

        self::assertFalse($d1->isLaterThan($d2));
        self::assertTrue($d2->isLaterThan($d1));

        self::assertTrue($d1->isLaterThanOrEquals($d1c));
        self::assertTrue($d1c->isLaterThanOrEquals($d1));
        self::assertTrue($d2->isLaterThanOrEquals($d1));
        self::assertFalse($d1->isLaterThanOrEquals($d2));
    }

    /**
     * @throws \PHPUnit\Framework\AssertionFailedError
     */
    public function testIntervals()
    {
        $intervalSince = $this->createMonth(4, 1945);
        $intervalUntil = $this->createMonth(6, 1945);
        $dx1 = $this->createMonth(4, 1945);
        $dx2 = $this->createMonth(5, 1945);
        $dx3 = $this->createMonth(6, 1945);
        $dy1 = $this->createMonth(3, 1945);
        $dy2 = $this->createMonth(7, 1945);

        self::assertTrue($dx1->isBetween($intervalSince, $intervalUntil));
        self::assertTrue($dx2->isBetween($intervalSince, $intervalUntil));
        self::assertTrue($dx3->isBetween($intervalSince, $intervalUntil));

        self::assertFalse($dx1->isBetweenExceptEquals($intervalSince, $intervalUntil));
        self::assertTrue($dx2->isBetween($intervalSince, $intervalUntil));
        self::assertFalse($dx3->isBetweenExceptEquals($intervalSince, $intervalUntil));

        self::assertFalse($dy1->isBetween($intervalSince, $intervalUntil));
        self::assertFalse($dy2->isBetween($intervalSince, $intervalUntil));
    }

    public function testMonthShift()
    {
        $d = $this->createMonth(4, 2019);
        $dPlus = $d->addMonth();
        self::assertEquals('05/2019', $dPlus->__toString());
        $d = $this->createMonth(4, 2019);
        $dMin = $d->subMonth();
        self::assertEquals('03/2019', $dMin->__toString());
    }

    /**
     * @throws \PHPUnit\Framework\AssertionFailedError
     */
    public function testDays()
    {
        $d = $this->createMonth(4, 2019);
        try {
            $d1 = new JulianDate(1, 4, 2019);
            $d2 = new JulianDate(30, 4, 2019);
        }
        catch (InvalidDateException|UnsupportedDateException $e) {
            throw new \LogicException();
        }
        self::assertTrue($d1->equals($d->getFirstDay()));
        self::assertTrue($d2->equals($d->getLastDay()));
    }

    /**
     * @throws UnsupportedDateTimeException
     */
    public function testSeconds()
    {
        $d = $this->createMonth(4, 2019);
        try {
            $d1 = new ExtendedDatetime('2019-04-01 00:00:00', new \DateTimeZone('UTC'));
            $d2 = new ExtendedDatetime('2019-04-30 23:59:59', new \DateTimeZone('UTC'));
        }
        catch (UnsupportedDateTimeException $e) {
            throw new \LogicException();
        }
        self::assertEquals($d1->__toString(), $d->getFirstSecond(new \DateTimeZone('UTC')));
        self::assertEquals($d2->__toString(), $d->getLastSecond(new \DateTimeZone('UTC')));
    }
}
