<?php

namespace MTi\UnitTest;

use MTi\DateTime\GregorianDate;
use MTi\InvalidDateException;
use MTi\UnsupportedDateException;
use PHPUnit\Framework\TestCase;


class GregorianDateTest
    extends TestCase
{
    private function cd($global, $month = NULL, $year = NULL)
    {
        try {
            return new GregorianDate($global, $month, $year);
        }
        catch (InvalidDateException|UnsupportedDateException $e) {
            throw new \LogicException();
        }
    }

    public function testCalendarSpecific()
    {
        self::assertEquals(
            '1900-03-01'
          , $this->cd(28, 2, 1900)->addDay()->isoDate()
        );
    }
}
