<?php

namespace MTi\UnitTest;

use MTi\DateTime\DateTime;
use MTi\DateTime\ExtendedDatetime;
use MTi\IDatetime;
use MTi\UnsupportedDateTimeException;
use PHPUnit\Framework\TestCase;


class ExtendedDatetimeTest
    extends TestCase
{
    private function makeDT($value): ExtendedDatetime
    {
        try {
            return new ExtendedDatetime($value, new \DateTimeZone('Europe/Prague'));
        }
        catch (UnsupportedDateTimeException $e) {
            throw new \LogicException();
        }
    }

    public function initializationPassProvider()
    {
        return [
            'infinity' => ['9999-12-31 23:59:59', 'infinity'],
            'database format' => ['1920-01-31 12:00:00', '1920-01-31 12:00:00'],
            'JSON format' => ['2020-07-15 01:59:13', '2020-07-15T01:59:13'],
            'leap february' => ['2016-02-29 23:00:00', '2016-02-29 23:00:00'],
            'M/D/Y (US) format' => ['1984-03-12 17:00:00', '03/12/1984 17:00'],
            'D.M.Y format' => ['2014-08-01 01:05:40', '1.8.2014 01:05:40'],
        ];
    }

    public function initializationInvalidProvider()
    {
        return [
            ['invalid'],
            ['now'],
            ['tomorrow'],
        ];
    }

    public function testAtomics()
    {
        $dt = $this->makeDT('2017-10-11 14:12:06');
        self::assertEquals(2017, $dt->year());
        self::assertEquals(10, $dt->month());
        self::assertEquals(11, $dt->day());
        self::assertEquals(14, $dt->hour());
        self::assertEquals(12, $dt->minute());
        self::assertEquals(6, $dt->second());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testInitInvalid()
    {
        $this->makeDT(null);
    }

    /**
     * @dataProvider initializationPassProvider
     *
     * @param $expected
     * @param $datestring
     */
    public function testFormatsPass($expected, $datestring)
    {
        self::assertEquals(
            $expected
          , ($this->makeDT($datestring))->isoDatetime()
        );
    }

    /**
     * @dataProvider initializationInvalidProvider
     * @expectedException \InvalidArgumentException
     *
     * @param $datestring
     */
    public function testFormatsInvalid($datestring)
    {
        $this->makeDT($datestring);
    }

    public function testEndOfDay()
    {
        $dt = $this->makeDT('2010-01-01 05:30:00');
        $dt->setEndOfDay();
        self::assertEquals('2010-01-01 23:59:59', $dt->isoDatetime());
    }

    public function testDate()
    {
        $dt = $this->makeDT('2010-01-01 05:30:00');
        self::assertEquals('2010-01-01', $dt->date()->isoDate());
    }

    /**
     * @throws \PHPUnit\Framework\AssertionFailedError
     */
    public function testInfinity()
    {
        $dt = $this->makeDT(IDatetime::INFINITY_DATETIME);
        self::assertTrue($dt->isInfinite());
    }

    /**
     * @throws \PHPUnit\Framework\Exception
     * @throws \Exception
     */
    public function testTransform()
    {
        $dateTimeString = '1968-08-19 21:15:00';
        $dt = new DateTime($dateTimeString);
        $jdt = $this->makeDT($dt);
        self::assertInstanceOf(ExtendedDatetime::class, $jdt);
        self::assertEquals($dateTimeString, $jdt->isoDatetime());
    }

    public function testJsonFormat()
    {
        $dt = $this->makeDT('1968-08-19 21:15:00');
        self::assertEquals('1968-08-19T21:15:00', $dt->jsonSerialize());
    }
}